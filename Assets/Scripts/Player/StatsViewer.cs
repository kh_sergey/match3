using UnityEngine;
using TMPro;

public class StatsViewer : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _scoreText;
    [SerializeField] private TextMeshProUGUI _turnsText;

    private PlayerStats _playerStats;

    private void Awake()
    {
        _playerStats = GetComponent<PlayerStats>();
    }

    private void Start()
    {
        UpdateScoreText();
        UpdateTurnsText();
    }

    private void OnEnable()
    {
        _playerStats.OnPointsChange += UpdateScoreText;
        _playerStats.OnTurnsChange += UpdateTurnsText;
    }

    private void OnDisable()
    {
        _playerStats.OnPointsChange -= UpdateScoreText;
        _playerStats.OnTurnsChange -= UpdateTurnsText;
    }

    private void UpdateScoreText()
    {
        _scoreText.text = _playerStats.Points.ToString();
    }

    private void UpdateTurnsText()
    {
        _turnsText.text = _playerStats.Turns.ToString();
    }
}
