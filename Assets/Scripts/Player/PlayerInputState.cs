using UnityEngine;
using UnityEngine.Events;

public class PlayerInputState : BaseState
{
    public event UnityAction<Tile> OnTileClicked;
    public event UnityAction OnMouseClicked;

    [SerializeField] private LayerMask _raycastLayerMask;

    private Pause _pause;
    private PlayerStats _playerStats;

    private bool _isInputAllowed = false;

    public override void EnterState()
    {
        _isInputAllowed = true;
    }

    public override void ExitState()
    {
        _isInputAllowed = false;
    }

    private void Awake()
    {
        _pause = GetComponent<Pause>();
        _playerStats = GetComponent<PlayerStats>();
    } 

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            OnMouseClicked?.Invoke();

            if (!_pause.IsPaused && _isInputAllowed)
            {
                ClickTile();
            }
        }
    }

    private void ClickTile()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit2D hit = Physics2D.GetRayIntersection(ray);
        if (hit.collider != null)
        {
            if (hit.collider.TryGetComponent(out Tile tile))
            {
                _isInputAllowed = false;
                _stateSwitcher.SwitchState<ExplosionState>();
                _playerStats.RemoveTurn();
                OnTileClicked?.Invoke(tile);
            }
        }
    }
}