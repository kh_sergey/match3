using UnityEngine;
using UnityEngine.Events;

public class PlayerStats : MonoBehaviour
{
    public event UnityAction OnPointsChange;
    public event UnityAction OnTurnsChange;
    public event UnityAction OnRanOutOfTurns;

    public int Points { get; private set; }
    public int Turns { get; private set; }

    [SerializeField] private int _initialTurnsCount = 10;

    private BallsEventManager _ballsEventManager;
    private TurnsCalculator _turnsCalculator;

    private void Awake()
    {
        _ballsEventManager = FindObjectOfType<BallsEventManager>();
        _turnsCalculator = GetComponent<TurnsCalculator>();
        AddTurns(_initialTurnsCount);
    }

    private void OnEnable()
    {
        _ballsEventManager.OnPointsEarning += AddPoints;
        _turnsCalculator.OnTurnsGaining += AddTurns;
    }

    private void OnDisable()
    {
        _ballsEventManager.OnPointsEarning -= AddPoints;
        _turnsCalculator.OnTurnsGaining -= AddTurns;
    }

    private void AddPoints(int value)
    {
        Points += value;
        OnPointsChange?.Invoke();
    }

    public void AddTurns(int value)
    {
        Turns += value;
        OnTurnsChange?.Invoke();
    }

    public void RemoveTurn()
    {
        Turns--;
        OnTurnsChange?.Invoke();

        if (Turns == 0)
        {
            OnRanOutOfTurns?.Invoke();
        }
    }
}
