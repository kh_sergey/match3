using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

public class TurnsCalculator : MonoBehaviour
{
    public event UnityAction<int> OnTurnsGaining;

    private ExplosionState _explosionState;

    private void Awake()
    {
        _explosionState = FindObjectOfType<ExplosionState>();
    }

    private void OnEnable()
    {
        _explosionState.OnBallsExploded += CalculateGainedTurns;
    }

    private void OnDisable()
    {
        _explosionState.OnBallsExploded -= CalculateGainedTurns;
    }

    private void CalculateGainedTurns(List<GameObject> explodedBalls)
    {
        int turns = 0;
        int explodedBallsCount = explodedBalls.Count;

        if (explodedBallsCount > 7)
        {
            turns = 4;
        }
        else if (explodedBallsCount >= 7)
        {
            turns = 3;
        }
        else if (explodedBallsCount >= 5)
        {
            turns = 2;
        }
        else if (explodedBallsCount >= 3)
        {
            turns = 1;
        }

        if (turns > 0)
        {
            OnTurnsGaining?.Invoke(turns);
        }
    }
}
