using UnityEngine;
using TMPro;

public class EndGameState : BaseState
{
    [SerializeField] private GameObject _mainMenuEndGameScreen;
    [SerializeField] private TextMeshProUGUI _mainMenuScoreText;
    [SerializeField] private GameObject _highScoreEndGameScreen;
    [SerializeField] private TextMeshProUGUI _highScoreScoreText;

    private HighScoreDataController _highScoreDataController;
    private PlayerStats _playerStats;

    public override void EnterState()
    {
        Lose();
    }

    public override void ExitState()
    {
    }

    private void Awake()
    {
        _playerStats = FindObjectOfType<PlayerStats>();
        _highScoreDataController = FindObjectOfType<HighScoreDataController>();
    }

    private void Lose()
    {
        ShowResults();
    }

    private void ShowResults()
    {
        TextMeshProUGUI scoreText;

        if(_playerStats.Points > _highScoreDataController.GetMinimalHighScore())
        {
            _highScoreEndGameScreen.SetActive(true);
            _highScoreDataController.AddHighScore(_playerStats.Points);
            scoreText = _highScoreScoreText;
        } 
        else
        {
            _mainMenuEndGameScreen.SetActive(true);
            scoreText = _mainMenuScoreText;
        }

        scoreText.text += _playerStats.Points.ToString();
    }

    public void TrySaveScore()
    {
        if(_playerStats.Points > _highScoreDataController.GetMinimalHighScore())
        {
            _highScoreDataController.AddHighScore(_playerStats.Points);
        }
    }
}
