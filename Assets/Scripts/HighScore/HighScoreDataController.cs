using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

public class HighScoreDataController : MonoBehaviour
{
    public int Rows { get; private set; } = 9;

    private List<HighScoreRow> _highScoreRows;

    private CSVExtracter _csvExtracter;

    private void Awake()
    {
        _csvExtracter = GetComponent<CSVExtracter>();

        CheckHighScoreInitialization();
        LoadHighScores();
    }

    public List<HighScoreRow> GetHighScoreRows()
    {
        SortHighScoreRows();
        
        return _highScoreRows;
    }

    private void CheckHighScoreInitialization()
    {
        if (!PlayerPrefs.HasKey(PlayerPrefsFields.IsHighScoreInitialized))
        {
            SaveCSVHighScores();
        }
    }

    private void SaveCSVHighScores()
    {
        _highScoreRows = _csvExtracter.Extract();
        SaveHighScores();
    }

    private class HighScoreList // For JsonUtility
    {
        public List<HighScoreRow> highScoreRows;
    }

    private void SaveHighScores()
    {
        HighScoreList highScoreList = new HighScoreList { highScoreRows = _highScoreRows };
        string jsonHighScoreList = JsonUtility.ToJson(highScoreList);
        PlayerPrefs.SetString(PlayerPrefsFields.HighScoreList, jsonHighScoreList);
        PlayerPrefs.Save();
    }

    private void LoadHighScores()
    {
        string jsonHighScoreList = PlayerPrefs.GetString(PlayerPrefsFields.HighScoreList);
        HighScoreList highScoreList = JsonUtility.FromJson<HighScoreList>(jsonHighScoreList);
        _highScoreRows = highScoreList.highScoreRows;
    }

    private void SortHighScoreRows()
    {
        for (int i = 0; i < _highScoreRows.Count; i++)
        {
            for (int j = i + 1; j < _highScoreRows.Count; j++)
            {
                if (_highScoreRows[j].Score > _highScoreRows[i].Score)
                {
                    var tmp = _highScoreRows[i];
                    _highScoreRows[i] = _highScoreRows[j];
                    _highScoreRows[j] = tmp;
                }
            }
        }

        _highScoreRows = _highScoreRows.Take(Rows).ToList();

        for (int i = 0; i < _highScoreRows.Count; i++)
        {
            _highScoreRows[i].SetRank(i + 1);
        }
    }

    public int GetMinimalHighScore()
    {
        LoadHighScores();
        SortHighScoreRows();
        return _highScoreRows[_highScoreRows.Count - 1].Score;
    }

    public void AddHighScore(int score)
    {
        string date = DateTime.Now.Date.ToString("dd.MM.yyyy");
        _highScoreRows.Add(new HighScoreRow(score, date));
        SortHighScoreRows();
        SaveHighScores();
    }
}
