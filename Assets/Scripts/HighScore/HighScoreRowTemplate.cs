using UnityEngine;
using System;
using TMPro;

public class HighScoreRowTemplate : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _rankText;
    [SerializeField] private TextMeshProUGUI _scoreText;
    [SerializeField] private TextMeshProUGUI _dateText;

    public void Initialize(HighScoreRow highScoreRow)
    {
        _rankText.text = highScoreRow.Rank.ToString();
        _scoreText.text = highScoreRow.Score.ToString();
        _dateText.text = highScoreRow.Date.ToString();
    }
}
