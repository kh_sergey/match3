using System;

[Serializable]
public class HighScoreRow
{
    public int Rank; // jsonUtility does not support properties
    public int Score;
    public string Date;

    public HighScoreRow(int score, string date)
    {
        Score = score;
        Date = date;
    }

    public void SetRank(int rank)
    {
        Rank = rank;
    }
}
