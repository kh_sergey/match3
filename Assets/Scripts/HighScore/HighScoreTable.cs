using UnityEngine;
using System.Collections.Generic;

public class HighScoreTable : MonoBehaviour
{
    [SerializeField] private GameObject _highScoreRowTemplatePrefab;
    [SerializeField] private Transform _highScoreContainer;

    private List<HighScoreRowTemplate> _highScoreRowTemplates;
    private List<HighScoreRow> _highScoreRows;

    private float _offsetY = 330f;
    private float _distanceBetweenRows = 2f;

    private HighScoreDataController _highScoreDataController;

    private void Awake()
    {
        _highScoreDataController = GetComponent<HighScoreDataController>();
        _highScoreRowTemplates = new List<HighScoreRowTemplate>();
    }

    private void Start()
    {
        _highScoreRows = _highScoreDataController.GetHighScoreRows();
        SpawnTemplates();
    }

    private void SpawnTemplates()
    {
        for (int i = 0; i < _highScoreRows.Count; i++)
        {
            GameObject spawnedTemplate = Instantiate(_highScoreRowTemplatePrefab, _highScoreContainer);
            spawnedTemplate.GetComponent<HighScoreRowTemplate>().Initialize(_highScoreRows[i]);
            RectTransform spawnedTemplateRectTransform = spawnedTemplate.GetComponent<RectTransform>();
            spawnedTemplateRectTransform.anchoredPosition = new Vector2(0, (-(spawnedTemplateRectTransform.rect.height + _distanceBetweenRows) * i) + _offsetY);
        }
    }
}
