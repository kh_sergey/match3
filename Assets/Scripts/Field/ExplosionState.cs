using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;

public class ExplosionState : BaseState
{
    public event UnityAction<List<GameObject>> OnBallsExploded;

    private Matcher _matcher;
    private PlayerInputState _playerInputState;
    private BallsEventManager _ballsEventManager;

    private List<GameObject> _explodedBalls;

    public override void EnterState()
    {
        _playerInputState.OnTileClicked += ExplodeBallMatches;
        _ballsEventManager.OnPointsTextShowing += ExtendStateForTime;
        OnExtendedStateTimeEnded += DespawnExplodedBalls;
    }

    public override void ExitState()
    {
        _playerInputState.OnTileClicked -= ExplodeBallMatches;
        _ballsEventManager.OnPointsTextShowing -= ExtendStateForTime;
        OnExtendedStateTimeEnded -= DespawnExplodedBalls;
    }

    private void Awake()
    {
        _matcher = GetComponent<Matcher>();
        _playerInputState = FindObjectOfType<PlayerInputState>();
        _ballsEventManager = GetComponent<BallsEventManager>();
    }

    public void ExplodeBallMatches(Tile tile) // ��� ����������� ��������������� ���� �����������
    {
        List<Tile> matches = _matcher.FindTileBallMatches(tile);
        _explodedBalls = new List<GameObject>();

        foreach (Tile matchingTile in matches)
        {
            _explodedBalls.Add(matchingTile.BallActions.gameObject);
            matchingTile.ExplodeBall();
        }
    }

    private void DespawnExplodedBalls()
    {
        _stateSwitcher.SwitchState<DespawnState>();
        OnBallsExploded?.Invoke(_explodedBalls);
    }
}
