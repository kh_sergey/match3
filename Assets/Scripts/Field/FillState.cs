using UnityEngine;

public class FillState : BaseState
{
    [SerializeField] private Ball[] _balls;

    private BallsEventManager _ballsEventManager;
    private DespawnState _ballsDespawner;
    private Field _field;
    private PlayerStats _playerStats;

    public override void EnterState()
    {
        _ballsEventManager.OnBallMoving += ExtendStateForTime;
        OnExtendedStateTimeEnded += LoadNextState;
        FillField();
    }

    public override void ExitState()
    {
        _ballsEventManager.OnBallMoving -= ExtendStateForTime;
        OnExtendedStateTimeEnded -= LoadNextState;
    }

    private void Awake()
    {
        _ballsEventManager = GetComponent<BallsEventManager>();
        _ballsDespawner = GetComponent<DespawnState>();
        _field = GetComponent<Field>();
        _playerStats = FindObjectOfType<PlayerStats>();
    }

    public void FillField()
    {
        for (int i = 0; i < _field.TilesSet.GetLength(0); i++)
        {
            for (int j = 0; j < _field.TilesSet.GetLength(1); j++)
            {
                if (_field.TilesSet[i,j].Ball == null)
                {
                    SpawnBallInTile(_field.TilesSet[i,j]);
                }
            }
        }
    }

    private void SpawnBallInTile(Tile tile)
    {
        int randomBallNumber = Random.Range(0, _balls.Length);

        GameObject spawnedBall = _ballsDespawner.PopDespawnedBall();
        spawnedBall.GetComponent<BallInitializer>().Initialize(_balls[randomBallNumber]);
        Vector2 spawnPosition = new Vector2(tile.transform.position.x, tile.transform.position.y + _field.Rows);
        spawnedBall.name = spawnedBall.GetComponent<BallInitializer>().Ball.Name;
        spawnedBall.transform.position = spawnPosition;

        tile.SetBall(spawnedBall);
    }

    private void LoadNextState()
    {
        if (_playerStats.Turns == 0)
        {
            _stateSwitcher.SwitchState<EndGameState>();
        }
        else
        {
            if (!PlayerPrefs.HasKey(PlayerPrefsFields.IsTutorialShowed))
            {
                _stateSwitcher.SwitchState<TutorialState>();
            }
            else
            {
                _stateSwitcher.SwitchState<PlayerInputState>();
            }
        }
    }
}
