using UnityEngine;
using System.Collections.Generic;

public class Matcher : MonoBehaviour
{
    private Field _field;

    private List<Tile> _searchTiles;
    private List<Tile> _matchesList;

    private void Awake()
    {
        _field = GetComponent<Field>();
    }

    public List<Tile> FindTileBallMatches(Tile point)
    {
        _matchesList = new List<Tile>();
        _searchTiles = new List<Tile>();

        _searchTiles.Add(point);

        while (_searchTiles.Count != 0)
        {
            CheckMatchesAroundTile(_searchTiles[0]);
            _matchesList.Add(_searchTiles[0]);
            _searchTiles.RemoveAt(0);
        }

        return _matchesList;
    }

    private void CheckMatchesAroundTile(Tile tile)
    {
        TryCheckMatchWithTile(tile, tile.Column - 1, tile.Row);
        TryCheckMatchWithTile(tile, tile.Column + 1, tile.Row);
        TryCheckMatchWithTile(tile, tile.Column, tile.Row - 1);
        TryCheckMatchWithTile(tile, tile.Column, tile.Row + 1);
    }

    private void TryCheckMatchWithTile(Tile centerTile, int column, int row)
    {
        if (column >= 0 && column < _field.Columns)
        {
            if (row >= 0 && row < _field.Rows)
            {
                if (_field.TilesSet[column, row].Ball.Name == centerTile.Ball.Name)
                {
                    if (!_searchTiles.Contains(_field.TilesSet[column, row]) && !_matchesList.Contains(_field.TilesSet[column,row]))
                    {
                        _searchTiles.Add(_field.TilesSet[column, row]);
                    }
                }
            }
        }
    }
}
