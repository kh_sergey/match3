using UnityEngine;

public class Tile : MonoBehaviour
{
    public BallActions BallActions { get; private set; }
    public Ball Ball { get; private set; }

    public int Column { get; private set; }
    public int Row { get; private set; }

    public void Initialize(int column, int row)
    {
        Column = column;
        Row = row;
    }

    public void SetBall(GameObject ball)
    {
        BallActions = ball.GetComponent<BallActions>();
        Ball = ball.GetComponent<BallInitializer>().Ball;
        BallActions.MoveToTile(this);
        ball.transform.parent = transform;
    }

    public void PassBallToAnotherTile(Tile tile)
    {
        tile.SetBall(BallActions.gameObject);
        BallActions.MoveToTile(tile);
        BallActions = null;
        Ball = null;
    }

    public void ExplodeBall()
    {
        BallActions.Explode();
        BallActions = null;
        Ball = null;
    }
}
