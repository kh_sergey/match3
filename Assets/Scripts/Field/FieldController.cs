using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class FieldController : MonoBehaviour, IStateSwitcher
{
    [SerializeField] private List<BaseState> _allStates;

    private BaseState _currentState;
    private Field _field;
    private DespawnState _ballsDespawner;

    private void Awake()
    {
        _ballsDespawner = GetComponent<DespawnState>();
        _field = GetComponent<Field>();

        _field.InitializeTilesSet();
        _ballsDespawner.InstantiateDespawnedBalls();
    }

    private void Start()
    {
        foreach (BaseState state in _allStates)
        {
            state.Initialize(this);
        }

        _currentState = _allStates[0];
        SwitchState<FillState>();
    }

    public void SwitchState<T>() where T : BaseState
    {
        BaseState state = _allStates.FirstOrDefault(s => s is T);
        _currentState.ExitState();
        _currentState = state;
        _currentState.EnterState();
    }
}
