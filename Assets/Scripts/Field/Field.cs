using UnityEngine;

public class Field : MonoBehaviour
{
    public int Rows => _rows;
    public int Columns => _columns;

    [SerializeField] private int _rows;
    [SerializeField] private int _columns;
    [SerializeField] private GameObject _tilePrefab;
    [SerializeField] private GameObject _tilesParent;

    public Tile[,] TilesSet { get; private set; }

    public void InitializeTilesSet()
    {
        TilesSet = new Tile[_columns, _rows];

        for (int i = 0; i < _columns; i++)
        {
            for (int j = 0; j < _rows; j++)
            {
                InitializeTile(i, j);
            }
        }

        CenterTilesParent();
    }

    private void InitializeTile(int column, int row)
    {
        Vector2 tilePosition = new Vector2(column, row);
        GameObject spawnedTileObject = Instantiate(_tilePrefab, tilePosition, Quaternion.identity);
        Tile spawnedTile = spawnedTileObject.GetComponent<Tile>();
        spawnedTile.Initialize(column, row);
        spawnedTileObject.transform.parent = _tilesParent.transform;
        spawnedTileObject.name = $"[{column},{row}]";
        TilesSet[column, row] = spawnedTile;
    }

    private void CenterTilesParent()
    {
        // ����� ����� ���� �� ������ �� � �������?
        _tilesParent.transform.position = new Vector2(-((_columns / 2f) - 0.5f), -(_rows / 2f) + 0.2f);
    }
}
