public class CollapseState : BaseState
{
    private Field _field;
    private BallsEventManager _ballsEventManager;

    public override void EnterState()
    {
        _ballsEventManager.OnBallMoving += ExtendStateForTime;
        OnExtendedStateTimeEnded += FillEmptyTiles;
        CollapseAllEmptyTiles();
    }

    public override void ExitState()
    {
        _ballsEventManager.OnBallMoving -= ExtendStateForTime;
        OnExtendedStateTimeEnded -= FillEmptyTiles;
    }

    private void Awake()
    {
        _field = GetComponent<Field>();
        _ballsEventManager = GetComponent<BallsEventManager>();
    }

    public void CollapseAllEmptyTiles()
    {
        bool anyBallWasPassed = false;
        int emptyTilesInRow = 0;

        for (int i = 0; i < _field.Columns; i++)
        {
            for (int j = 0; j < _field.Rows; j++)
            {
                if (_field.TilesSet[i, j].Ball == null)
                {
                    emptyTilesInRow++;
                } 
                else if (emptyTilesInRow > 0)
                {
                    _field.TilesSet[i, j].PassBallToAnotherTile(_field.TilesSet[i, j - emptyTilesInRow]);
                    anyBallWasPassed = true;
                }
            }
            emptyTilesInRow = 0;
        }

        if (anyBallWasPassed == false)
        {
            FillEmptyTiles();
        }
    }

    private void FillEmptyTiles()
    {
        _stateSwitcher.SwitchState<FillState>();
    }
}
