using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public abstract class BaseState : MonoBehaviour
{
    protected event UnityAction OnExtendedStateTimeEnded;

    protected IStateSwitcher _stateSwitcher;
    protected Coroutine _extendStateForTime;

    public abstract void EnterState();
    public abstract void ExitState();

    public void Initialize(IStateSwitcher stateSwitcher)
    {
        _stateSwitcher = stateSwitcher;
    }

    protected void ExtendStateForTime(float time)
    {
        if (_extendStateForTime != null)
        {
            StopCoroutine(_extendStateForTime);
        }
        _extendStateForTime = StartCoroutine(ExtendStateForTimeProcess(time));
    }

    protected IEnumerator ExtendStateForTimeProcess(float time)
    {
        yield return new WaitForSeconds(time);

        OnExtendedStateTimeEnded?.Invoke();
    }
}
