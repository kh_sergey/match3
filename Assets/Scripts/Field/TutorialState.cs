using UnityEngine;
using System.Collections.Generic;

public class TutorialState : BaseState
{
    [SerializeField] private GameObject _helper;

    private Matcher _matcher;
    private Field _field;
    private PlayerInputState _playerInputState;

    private List<Tile> _biggestMatch;

    public override void EnterState()
    {
        _playerInputState.OnMouseClicked += End;
        Play();
    }

    public override void ExitState()
    {
        _playerInputState.OnMouseClicked -= End;
    }

    private void Awake()
    {
        _biggestMatch = new List<Tile>();
        _matcher = FindObjectOfType<Matcher>();
        _field = FindObjectOfType<Field>();
        _playerInputState = FindObjectOfType<PlayerInputState>();
    }

    public void Play()
    {
        FindBiggestMatch();
        HighlightBiggestMatch();
    }

    private void HighlightBiggestMatch()
    {
        foreach(Tile tile in _biggestMatch)
        {
            tile.BallActions.StartHighlight();
        }

        _helper.SetActive(true);
        _helper.transform.position = _biggestMatch[0].transform.position;
    }

    private void DisableHighlight()
    {
        foreach (Tile tile in _biggestMatch)
        {
            tile.BallActions.StopHighlight();
        }

        _helper.SetActive(false);

        _biggestMatch = new List<Tile>();
    }

    private void FindBiggestMatch()
    {
        List<Tile> matches;

        for (int i = 0; i < _field.TilesSet.GetLength(0); i++)
        {
            for (int j = 0; j < _field.TilesSet.GetLength(1); j++)
            {
                matches = _matcher.FindTileBallMatches(_field.TilesSet[i, j]);

                if (matches.Count > _biggestMatch.Count)
                {
                    _biggestMatch = matches;
                }
            }
        }
    }

    private void End()
    {
        DisableHighlight();
        PlayerPrefs.SetInt(PlayerPrefsFields.IsTutorialShowed, 1);
        _stateSwitcher.SwitchState<PlayerInputState>();
    }
}
