using UnityEngine;
using System.Collections.Generic;

public class DespawnState : BaseState
{
    public Stack<GameObject> DespawnedBalls { get; private set; }

    [SerializeField] private GameObject _ballPrefab;
    [SerializeField] private GameObject _despawnedBallsParent;

    private Field _field;
    private ExplosionState _explosionState;

    public override void EnterState()
    {
        _explosionState.OnBallsExploded += DespawnBalls;
    }

    public override void ExitState()
    {
        _explosionState.OnBallsExploded -= DespawnBalls;
    }

    private void Awake()
    {
        _explosionState = GetComponent<ExplosionState>();
    }

    private void DespawnBalls(List<GameObject> balls)
    {
        foreach(GameObject ball in balls)
        {
            DespawnBall(ball);
        }

        _stateSwitcher.SwitchState<CollapseState>();
    }

    public void InstantiateDespawnedBalls()
    {
        _field = GetComponent<Field>();
        DespawnedBalls = new Stack<GameObject>();

        for (int i = 0; i < _field.Columns * _field.Rows; i++)
        {
            GameObject spawnedBall = Instantiate(_ballPrefab);
            spawnedBall.transform.parent = _despawnedBallsParent.transform;
            DespawnedBalls.Push(spawnedBall);
        }
    }

    public void DespawnBall(GameObject ball)
    {
        ball.GetComponent<BallInitializer>().Initialize(null);
        ball.transform.parent = _despawnedBallsParent.transform;
        ball.transform.position = _despawnedBallsParent.transform.position;
        ball.name = _ballPrefab.name;

        DespawnedBalls.Push(ball);
    }

    public GameObject PopDespawnedBall()
    {
        return DespawnedBalls.Pop();
    }
}
