using UnityEngine;
using System;

public class Pause : MonoBehaviour
{
    public bool IsPaused { get; private set; } = false;

    private SceneLoader _sceneLoader;

    private void Awake()
    {
        _sceneLoader = FindObjectOfType<SceneLoader>();
    }

    private void OnEnable()
    {
        _sceneLoader.OnLoadScene += DisablePause;
    }

    private void OnDisable()
    {
        _sceneLoader.OnLoadScene -= DisablePause;
    }

    private void DisablePause()
    {
        TogglePause(false);
    }

    public void TogglePause(bool state)
    {
        Time.timeScale = Convert.ToInt32(!state);

        IsPaused = state;
    }
}
