using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public event UnityAction OnLoadScene;

    public enum Scenes
    {
        MainMenu = 0,
        Gameplay = 1,
        HighScoreTable = 2,
        Credits = 3
    }

    public void LoadMainMenu() 
    {
        LoadScene(Scenes.MainMenu);
    }

    public void LoadGameplay()
    {
        LoadScene(Scenes.Gameplay);
    }

    public void LoadHighScoreTable()
    {
        LoadScene(Scenes.HighScoreTable);
    }

    public void LoadCredits()
    {
        LoadScene(Scenes.Credits);
    }

    private void LoadScene(Scenes scene)
    {
        OnLoadScene?.Invoke();

        SceneManager.LoadScene((int)scene);
    }
}
