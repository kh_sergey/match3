using UnityEngine;

[CreateAssetMenu(fileName = "New Ball", menuName = "Ball/Create new ball")]
public class Ball : ScriptableObject
{
    public Sprite Sprite => _sprite;
    public int PointsOnDestroy => _pointsOnDestroy;
    public string Name => _name;
    public AudioSource ExplosionSound => _explosionSound;
    public Animator ExplosionAnimation => _explosionAnimation;

    [SerializeField] private Sprite _sprite;
    [SerializeField] private int _pointsOnDestroy;
    [SerializeField] private string _name;
    [SerializeField] private AudioSource _explosionSound;
    [SerializeField] private Animator _explosionAnimation;
}
