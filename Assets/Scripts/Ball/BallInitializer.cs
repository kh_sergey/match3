using UnityEngine;

public class BallInitializer : MonoBehaviour
{
    public Ball Ball { get; private set; }

    private SpriteRenderer _spriteHolder;

    private void Awake()
    {
        _spriteHolder = GetComponentInChildren<SpriteRenderer>();
    }

    public void Initialize(Ball ball)
    {
        Ball = ball;

        if (ball == null)
        {
            _spriteHolder.sprite = null;
        }
        else
        {
            _spriteHolder.sprite = ball.Sprite;
        }
    }
}
