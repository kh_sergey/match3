using UnityEngine;
using System.Collections;
using DG.Tweening;
using TMPro;

public class BallActions : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _renderer;
    [SerializeField] private TextMeshProUGUI _pointsText;
    [SerializeField] private float _moveDuration = 0.5f;
    [SerializeField] private float _pointsTextDuration = 0.5f;
    [SerializeField] private float _explodeDuration = 0.25f;
    [SerializeField] private float _highlightDuration = 0.5f;

    private BallInitializer _ballInitializer;
    private BallsEventManager _ballsEventManager;

    private Sequence _movementSequence;
    private Sequence _highlightSequence;

    private void Awake()
    {
        _ballsEventManager = FindObjectOfType<BallsEventManager>();
        _ballInitializer = GetComponent<BallInitializer>();
    }

    public void Explode()
    {
        PlayExplodeAnimation();
        GivePoints(_ballInitializer.Ball.PointsOnDestroy);
    }

    public void MoveToTile(Tile tile)
    {
        _movementSequence.Kill();
        _movementSequence = DOTween.Sequence();
        _movementSequence.Append(transform.DOMoveY(tile.transform.position.y, _moveDuration));
        _ballsEventManager.InvokeBallMoving(_moveDuration);
    }

    private void GivePoints(int value)
    {
        _ballsEventManager.InvokePointsEarning(value);
        StartCoroutine(ShowAndHideGainedPoints(value));
        _ballsEventManager.InvokePointsTextShowing(_pointsTextDuration);
    }

    private IEnumerator ShowAndHideGainedPoints(int points)
    {
        _pointsText.gameObject.SetActive(true);
        _pointsText.text = points.ToString();

        yield return new WaitForSeconds(_pointsTextDuration);

        _pointsText.gameObject.SetActive(false);
    }

    public void PlayExplodeAnimation()
    {
        Vector2 originalScale = _renderer.transform.localScale;
        _renderer.transform.DOScale(new Vector2(0.1f, 0.1f), _explodeDuration)
            .OnComplete(() => { 
                _renderer.sprite = null;
                _renderer.transform.localScale = originalScale; 
            });
    }

    public void StartHighlight()
    {
        Vector2 originalScale = _renderer.transform.localScale;
        _highlightSequence.Kill();
        _highlightSequence = DOTween.Sequence();
        _highlightSequence.Append(_renderer.transform.DOScale(new Vector2(0.4f, 0.4f), _highlightDuration)
            .SetLoops(999, LoopType.Yoyo)
            .OnComplete(() => { _renderer.transform.localScale = originalScale; }));
    }

    public void StopHighlight()
    {
        _highlightSequence.Complete();
    }
}
