using UnityEngine;
using UnityEngine.Events;

public class BallsEventManager : MonoBehaviour
{
    public event UnityAction<float> OnBallMoving;
    public event UnityAction<int> OnPointsEarning;
    public event UnityAction<float> OnPointsTextShowing;

    public void InvokeBallMoving(float movingTime)
    {
        OnBallMoving?.Invoke(movingTime);
    }

    public void InvokePointsEarning(int points)
    {
        OnPointsEarning?.Invoke(points);
    }

    public void InvokePointsTextShowing(float pointsTextDuration)
    {
        OnPointsTextShowing?.Invoke(pointsTextDuration);
    }
}
