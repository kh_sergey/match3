public static class PlayerPrefsFields
{
    public static string HighScoreList = "HighScoreList";
    public static string IsTutorialShowed = "IsTutorialShowed";
    public static string IsHighScoreInitialized = "IsHighScoreInitialized";
}
