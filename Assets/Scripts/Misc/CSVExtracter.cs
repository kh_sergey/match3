using System.Collections.Generic;
using UnityEngine;
using System;

public class CSVExtracter : MonoBehaviour
{
    [SerializeField] private TextAsset csvFile;

    public List<HighScoreRow> Extract()
    {
        string[] data = csvFile.text.Split(new string[] { ";", "\n" }, StringSplitOptions.None);

        int tableSize = data.Length / 2 - 1;
        List<HighScoreRow> highScoreRows = new List<HighScoreRow>();

        for (int i = 0; i < tableSize; i++)
        {
            int score = int.Parse(data[2 * (i + 1)]);
            string date = data[2 * (i + 1) + 1];

            highScoreRows.Add(new HighScoreRow(score, date));
        }

        PlayerPrefs.SetInt(PlayerPrefsFields.IsHighScoreInitialized, 1);

        return highScoreRows;
    }
}
